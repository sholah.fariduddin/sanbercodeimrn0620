// SOAL 1 Looping While
console.log("SOAL 1")

var i = 1
console.log("LOOPING PERTAMA")
while (i < 21) {
    if (i % 2 === 0) {
        console.log(i + ' - I love coding')
    }
    i++
}
// sekarang i = 21
console.log("LOOPING KEDUA")
while (i > 0) {
    if (i % 2 === 0) {
        console.log(i + ' - I will become a mobile developer')
    }
    i--
}
console.log()

// No. 2 Looping menggunakan for
console.log("SOAL 2")
console.log("OUTPUT")
for (i = 1; i < 21; i++) {
    if ((i % 3 === 0) && (i % 2 !== 0)) {
        console.log(i + ' - I Love Coding')
    } else if (i % 2 !== 0) {
        console.log(i + ' - Santai')
    } else if (i % 2 === 0) {
        console.log(i + ' - Berkualitas')
    }
}
console.log()

// No. 3 Membuat Persegi Panjang
console.log("SOAL 3")
for (i = 0; i < 4; i++) {
    console.log('########')
}
console.log()

// No. 4 Membuat Tangga
console.log("SOAL 4")
var j = 0
for (i = 1; i < 8; i++) {
    for (j = 0; j < i; j++) {
        process.stdout.write("#") //untuk print kesamping
    }
    console.log()
}
console.log()

//No. 5 Membuat Papan Catur
console.log("SOAL 5")
for (i = 0; i < 8; i++) {
    if (i % 2 === 0) {
        console.log(' # # # #')
    } else {
        console.log('# # # #')
    }
}