// percobaan hello word
var sayHello = "Hello World!"
console.log(sayHello)

//Variable
var dia = "John" // Tipe
var umur = 12
var muda = false
console.log(dia) // "John"
console.log(umur) // 12
console.log(muda) // false

//Operator
var angka = 100
console.log(angka == 100) // true
console.log(angka == 80) // false

//String
var sentences = "Javascript"
console.log(sentences[0]) // "J"
console.log(sentences[2]) // "v"
console.log(sentences[5]) // "c"

//string 2
var car1 = 'Lykan Hypersport';
var car2 = car1.substr(1);
console.log(car2); // Hypersport

var motor1 = 'zelda motor';
var motor2 = motor1.substr(2, 3);
console.log(motor2); // ld

//tugas
var sentence = "I am going to be React Native Developer";
var exampleFirstWord = sentence[0];
var exampleSecondWord = sentence[2] + sentence[3];
console.log('First Word: ' + exampleFirstWord);

//conditional
var minimarketStatus = "close"
var minuteRemainingToOpen = 5
if (minimarketStatus == "open") {
    console.log("saya akan membeli telur dan buah")
} else if (minuteRemainingToOpen <= 5) {
    console.log("minimarket buka sebentar lagi, saya tungguin")
} else {
    console.log("minimarket tutup, saya pulang lagi")
}