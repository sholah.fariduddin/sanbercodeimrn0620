//Tugas Conditional - If-else
var nama = "Smith"
var peran = "Penyihir"
if (nama === "") {
    console.log("Peran Harus Diisi!")
    if (nama === "" || peran === "")
        console.log("Nama dan Peranmu Harus Diisi!")
    if (nama === "Smith") {
        console.log("Selamat Datang di Dunia Werewolf, " + nama)
    } else if (nama === "Smith" || peran === "Penyihir")
        console.log(`Halo${peran}${nama}, Kamu dapat melihat siapa yang menjadi werewolf!`)
    if (peran === "Werewolf") {
        console.log(`Halo Werewolf${nama}, Kamu dapat memangsa setiap saat!`)
    } else if (peran === "Guard")
        console.log(`Halo Werewolf${nama}, Kamu akan membantu teman-temanmu!`)
}

//Switch Case
var tahun = 1945;
var bulan = 5;
var tanggal = 5;
var teksbulan;
switch (true) {
    case (hari < 1 || hari > 31):
        console.log("Input Hari Salah!");
        break;
    case (bulan < 1 || bulan > 12):
        console.log("Input Bulan Salah!");
        break;
    case (tahun < 1900 || tahun > 2000):
        console.log("Input Tahun Salah!");
        break;
    default:
        switch (bulan) {
            case 1:
                teksbulan = "januari";
                console.log(hari + "" + teksbulan + "" + tahun);
                break;
            case 2:
                teksbulan = "februari";
                console.log(hari + "" + teksbulan + "" + tahun);
                break;
            case 3:
                teksbulan = "Maret";
                console.log(hari + "" + teksbulan + "" + tahun);
                break;
            case 4:
                teksbulan = "April";
                console.log(hari + "" + teksbulan + "" + tahun);
                break;
            case 5:
                teksbulan = "Mei";
                console.log(hari + "" + teksbulan + "" + tahun);
                break;
            case 6:
                teksbulan = "Juni";
                console.log(hari + "" + teksbulan + "" + tahun);
                break;
            case 7:
                teksbulan = "Juli";
                console.log(hari + "" + teksbulan + "" + tahun);
                break;
            case 8:
                teksbulan = "Agustus";
                console.log(hari + "" + teksbulan + "" + tahun);
                break;
            case 9:
                teksbulan = "September";
                console.log(hari + "" + teksbulan + "" + tahun);
                break;
            case 10:
                teksbulan = "oktober";
                console.log(hari + "" + teksbulan + "" + tahun);
                break;
            case 11:
                teksbulan = "November";
                console.log(hari + "" + teksbulan + "" + tahun);
                break;
            case 12:
                teksbulan = "Desember";
                console.log(hari + "" + teksbulan + "" + tahun);
                break;
            default:
                console.log("Tidak ada bulan seperti yang diinput!");
                break;
        }
        break;
}