//No 1
function teriak() {
    return "Halo sanbers!"
}
console.log(teriak())

//No 2
function kalikan(num1, num2) {
    return num1 * num2
}

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

//No 3

function introduce(Nama, age, adderes, hobby) {
    return `Nama saya ${Nama} umur saya ${age}tahun, alamat saya ${adderes} dan saya punya hobby yaitu ${hobby}!`
}

var Nama = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan = introduce('Agus', '30', 'Jln.Malioboro, Yogyakarta', 'Gaming');
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Maliob oro, Yogyakarta, dan saya punya hobby yaitu Gaming!"