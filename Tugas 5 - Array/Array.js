// No. 1 range ke 1
console.log("Nomer 1")
console.log("range (1, 10)");

function range(startNum, finishNum) {
    var arr = [];
    if (startNum == undefined || finishNum == undefined) {
        console.log(-1);
    } else if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            (arr.push(i));
        }
    } else {
        for (var i = startNum; i <= finishNum; i--) {
            (arr.push(i));
        }
    }
    return arr;
}
console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

console.log("range (1)");

function range(startNum, finishNum) {
    var arr = [];
    if (startNum == undefined || finishNum == undefined) {
        return -1;
    } else if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            (arr.push(i));
        }
    } else {
        for (var i = startNum; i <= finishNum; i--) {
            (arr.push(i));
        }
    }
    return arr;
}
console.log(range(1)); // -1

console.log("range (10, 18)");

function range(startNum, finishNum) {
    var arr = [];
    if (startNum == undefined || finishNum == undefined) {
        console.log(-1);
    } else if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            (arr.push(i));
        }
    } else {
        for (var i = startNum; i <= finishNum; i--) {
            (arr.push(i));
        }
    }
    return arr;
}
console.log(range(10, 18)); //[10, 11, 12, 13, 14, 15, 16, 17, 18]

console.log("range (54, 50)");

function range(startNum, finishNum) {
    var arr = [];
    if (startNum == undefined || finishNum == undefined) {
        console.log(-1);
    } else if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            (arr.push(i));
        }
    } else {
        for (var i = startNum; i >= finishNum; i--) {
            (arr.push(i));
        }
    }
    return arr;
}
console.log(range(54, 50)); // [54, 53, 52, 51, 50]

console.log("range ()")

function range(startNum, finishNum) {
    var arr = [];
    if (startNum == undefined || finishNum == undefined) {
        return -1;
    } else if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            (arr.push(i));
        }
    } else {
        for (var i = startNum; i <= finishNum; i--) {
            (arr.push(i));
        }
    }
    return arr;
}
console.log(range()); // -1

console.log("Nomer 2 Range with step");

function arrwithstep(num1, num2, step) {
    var arr1 = [];
    if (num1 < num2) {
        for (let j = num1; j <= num2; j += step) {
            arr1.push(j);
        }
        return arr1;
    } else if (num1 > num2) {
        for (let k = num1; k >= num2; k -= step) {
            (arr1.push(k));
        }
        return arr1;
    } else {
        return arr1
    }
}
console.log(arrwithstep(1, 10, 2));
console.log(arrwithstep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(arrwithstep(5, 2, 1)) // [5, 4, 3, 2]
console.log(arrwithstep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]