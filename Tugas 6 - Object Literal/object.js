var now = new Date()
var thisYear = now.getFullYear()
var age
var tahunstring
var tahunint
var namalengkap = {}

function arrayToObject(arr) {
    if (arr[0] == null) {
        console.log('" "')
    } else {
        for (var index = 0; index < arr.length; index++) {
            if (arr[index][3] == null || arr[index][3] == "undefined" || arr[index][3] > thisYear) {
                tahunstring = "invalid Brith Year"
            } else {
                tahunint = thisYear - arr[index][3]
                tahunstring = tahunint
            }
            var detail = {}
            detail.fristName = arr[index][0]
            detail.lastName = arr[index][1]
            detail.gender = arr[index][2]
            detail.age = tahunstring
            namalengkap[arr[index][0] + arr[index][1]] = detail
            console.log(namalengkap)
            namalengkap = {}
            detail = {}
            // Code di sini 
        }
    }
}

// Driver Code
var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

console.log("Nomer 2")

var
const name = new type(arguments);

function shoppingTime(memberId, money) {
    var sisa
    if (memberId == null || memberId == '') {
        return "Mohon maaf, Toko ini hanya berlaku untuk member saja"
    } else if (money < 50000) {
        return "Mohon maaf, Uang anda tidak cukup" // you can only write your code here!
    }
    namapelanggan.memberId = memberId
    namapelanggan.money = money
    sisa = namapelanggan.money
    if (sisa > 1500000) {
        arr2.push("'Sepatu Stacattu'")
        sisa -= 1500000
    }
    if (sisa > 500000) {
        arr2.push("'Baju Zoro'")
        sisa -= 500000
    }
    if (sisa > 250000) {
        arr2.push("'Baju H&N'")
        sisa -= 250000
    }
    if (sisa > 175000) {
        arr2.push("'Sweater Uniklooh'")
        sisa -= 175000
    }
    if (sia > 50000) {
        arr2.push("'Casing Handphone'")
        sia -= 50000
    }
    namapelanggan.listPurchased = arr2
    namapelanggan.changemoney = sisa
    arr2 = []
    sisa = 0
    retrun(namapelanggan)
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja